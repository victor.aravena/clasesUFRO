/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Persona {
	public Persona() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_PERSONA_RESERVA) {
			return ORM_reserva;
		}
		else if (key == orm.ORMConstants.KEY_PERSONA_RCE_EXAMEN) {
			return ORM_rce_examen;
		}
		else if (key == orm.ORMConstants.KEY_PERSONA_BITACORA) {
			return ORM_bitacora;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private int id;
	
	private String rut;
	
	private String nombres;
	
	private String apellido_paterno;
	
	private String apellido_materno;
	
	private String email;
	
	private java.util.Date fecha_nacimiento;
	
	private orm.Medico medico;
	
	private orm.Paciente paciente;
	
	private orm.Director director;
	
	private java.util.Set ORM_reserva = new java.util.HashSet();
	
	private java.util.Set ORM_rce_examen = new java.util.HashSet();
	
	private java.util.Set ORM_bitacora = new java.util.HashSet();
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setRut(String value) {
		this.rut = value;
	}
	
	public String getRut() {
		return rut;
	}
	
	public void setNombres(String value) {
		this.nombres = value;
	}
	
	public String getNombres() {
		return nombres;
	}
	
	public void setApellido_paterno(String value) {
		this.apellido_paterno = value;
	}
	
	public String getApellido_paterno() {
		return apellido_paterno;
	}
	
	public void setApellido_materno(String value) {
		this.apellido_materno = value;
	}
	
	public String getApellido_materno() {
		return apellido_materno;
	}
	
	public void setEmail(String value) {
		this.email = value;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setFecha_nacimiento(java.util.Date value) {
		this.fecha_nacimiento = value;
	}
	
	public java.util.Date getFecha_nacimiento() {
		return fecha_nacimiento;
	}
	
	public void setMedico(orm.Medico value) {
		if (this.medico != value) {
			orm.Medico lmedico = this.medico;
			this.medico = value;
			if (value != null) {
				medico.setPersona(this);
			}
			if (lmedico != null && lmedico.getPersona() == this) {
				lmedico.setPersona(null);
			}
		}
	}
	
	public orm.Medico getMedico() {
		return medico;
	}
	
	public void setPaciente(orm.Paciente value) {
		if (this.paciente != value) {
			orm.Paciente lpaciente = this.paciente;
			this.paciente = value;
			if (value != null) {
				paciente.setPersona(this);
			}
			if (lpaciente != null && lpaciente.getPersona() == this) {
				lpaciente.setPersona(null);
			}
		}
	}
	
	public orm.Paciente getPaciente() {
		return paciente;
	}
	
	public void setDirector(orm.Director value) {
		if (this.director != value) {
			orm.Director ldirector = this.director;
			this.director = value;
			if (value != null) {
				director.setPersona(this);
			}
			if (ldirector != null && ldirector.getPersona() == this) {
				ldirector.setPersona(null);
			}
		}
	}
	
	public orm.Director getDirector() {
		return director;
	}
	
	private void setORM_Reserva(java.util.Set value) {
		this.ORM_reserva = value;
	}
	
	private java.util.Set getORM_Reserva() {
		return ORM_reserva;
	}
	
	public final orm.ReservaSetCollection reserva = new orm.ReservaSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_PERSONA_RESERVA, orm.ORMConstants.KEY_RESERVA_PERSONA_ID_INGRESA_RESERVA, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Rce_examen(java.util.Set value) {
		this.ORM_rce_examen = value;
	}
	
	private java.util.Set getORM_Rce_examen() {
		return ORM_rce_examen;
	}
	
	public final orm.Rce_examenSetCollection rce_examen = new orm.Rce_examenSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_PERSONA_RCE_EXAMEN, orm.ORMConstants.KEY_RCE_EXAMEN_PERSONA, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Bitacora(java.util.Set value) {
		this.ORM_bitacora = value;
	}
	
	private java.util.Set getORM_Bitacora() {
		return ORM_bitacora;
	}
	
	public final orm.BitacoraSetCollection bitacora = new orm.BitacoraSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_PERSONA_BITACORA, orm.ORMConstants.KEY_BITACORA_PERSONA, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
