/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Rce_examen {
	public Rce_examen() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_RCE_EXAMEN_PAGO) {
			return ORM_pago;
		}
		else if (key == orm.ORMConstants.KEY_RCE_EXAMEN_RCE_EXAMEN_EXAMEN) {
			return ORM_rce_examen_examen;
		}
		else if (key == orm.ORMConstants.KEY_RCE_EXAMEN_BITACORA) {
			return ORM_bitacora;
		}
		
		return null;
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_RCE_EXAMEN_PROCEDENCIA) {
			this.procedencia = (orm.Procedencia) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_RCE_EXAMEN_RESERVA) {
			this.reserva = (orm.Reserva) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_RCE_EXAMEN_PERSONA) {
			this.persona = (orm.Persona) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private orm.Procedencia procedencia;
	
	private orm.Reserva reserva;
	
	private orm.Persona persona;
	
	private java.util.Set ORM_pago = new java.util.HashSet();
	
	private java.util.Set ORM_rce_examen_examen = new java.util.HashSet();
	
	private java.util.Set ORM_bitacora = new java.util.HashSet();
	
	/**
	 * Clave Primaria
	 */
	private void setId(int value) {
		this.id = value;
	}
	
	/**
	 * Clave Primaria
	 */
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setProcedencia(orm.Procedencia value) {
		if (procedencia != null) {
			procedencia.rce_examen.remove(this);
		}
		if (value != null) {
			value.rce_examen.add(this);
		}
	}
	
	public orm.Procedencia getProcedencia() {
		return procedencia;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Procedencia(orm.Procedencia value) {
		this.procedencia = value;
	}
	
	private orm.Procedencia getORM_Procedencia() {
		return procedencia;
	}
	
	public void setReserva(orm.Reserva value) {
		if (this.reserva != value) {
			orm.Reserva lreserva = this.reserva;
			this.reserva = value;
			if (value != null) {
				reserva.setRce_examen(this);
			}
			if (lreserva != null && lreserva.getRce_examen() == this) {
				lreserva.setRce_examen(null);
			}
		}
	}
	
	public orm.Reserva getReserva() {
		return reserva;
	}
	
	public void setPersona(orm.Persona value) {
		if (persona != null) {
			persona.rce_examen.remove(this);
		}
		if (value != null) {
			value.rce_examen.add(this);
		}
	}
	
	public orm.Persona getPersona() {
		return persona;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Persona(orm.Persona value) {
		this.persona = value;
	}
	
	private orm.Persona getORM_Persona() {
		return persona;
	}
	
	private void setORM_Pago(java.util.Set value) {
		this.ORM_pago = value;
	}
	
	private java.util.Set getORM_Pago() {
		return ORM_pago;
	}
	
	public final orm.PagoSetCollection pago = new orm.PagoSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_RCE_EXAMEN_PAGO, orm.ORMConstants.KEY_PAGO_RCE_EXAMEN, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Rce_examen_examen(java.util.Set value) {
		this.ORM_rce_examen_examen = value;
	}
	
	private java.util.Set getORM_Rce_examen_examen() {
		return ORM_rce_examen_examen;
	}
	
	public final orm.Rce_examen_examenSetCollection rce_examen_examen = new orm.Rce_examen_examenSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_RCE_EXAMEN_RCE_EXAMEN_EXAMEN, orm.ORMConstants.KEY_RCE_EXAMEN_EXAMEN_RCE_EXAMEN, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_Bitacora(java.util.Set value) {
		this.ORM_bitacora = value;
	}
	
	private java.util.Set getORM_Bitacora() {
		return ORM_bitacora;
	}
	
	public final orm.BitacoraSetCollection bitacora = new orm.BitacoraSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_RCE_EXAMEN_BITACORA, orm.ORMConstants.KEY_BITACORA_RCE_EXAMEN, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
