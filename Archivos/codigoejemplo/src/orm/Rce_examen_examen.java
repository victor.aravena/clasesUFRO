/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Rce_examen_examen {
	public Rce_examen_examen() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_RCE_EXAMEN_EXAMEN_RCE_EXAMEN) {
			this.rce_examen = (orm.Rce_examen) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_RCE_EXAMEN_EXAMEN_EXAMEN) {
			this.examen = (orm.Examen) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private orm.Rce_examen rce_examen;
	
	private orm.Examen examen;
	
	private Double monto;
	
	/**
	 * Claver primaria
	 */
	private void setId(int value) {
		this.id = value;
	}
	
	/**
	 * Claver primaria
	 */
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	/**
	 * Monto de Examen
	 */
	public void setMonto(double value) {
		setMonto(new Double(value));
	}
	
	/**
	 * Monto de Examen
	 */
	public void setMonto(Double value) {
		this.monto = value;
	}
	
	/**
	 * Monto de Examen
	 */
	public Double getMonto() {
		return monto;
	}
	
	public void setRce_examen(orm.Rce_examen value) {
		if (rce_examen != null) {
			rce_examen.rce_examen_examen.remove(this);
		}
		if (value != null) {
			value.rce_examen_examen.add(this);
		}
	}
	
	public orm.Rce_examen getRce_examen() {
		return rce_examen;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Rce_examen(orm.Rce_examen value) {
		this.rce_examen = value;
	}
	
	private orm.Rce_examen getORM_Rce_examen() {
		return rce_examen;
	}
	
	public void setExamen(orm.Examen value) {
		if (examen != null) {
			examen.rce_examen_examen.remove(this);
		}
		if (value != null) {
			value.rce_examen_examen.add(this);
		}
	}
	
	public orm.Examen getExamen() {
		return examen;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Examen(orm.Examen value) {
		this.examen = value;
	}
	
	private orm.Examen getORM_Examen() {
		return examen;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
