/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class Rce_examenDAO {
	public static Rce_examen loadRce_examenByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return loadRce_examenByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen getRce_examenByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return getRce_examenByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen loadRce_examenByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return loadRce_examenByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen getRce_examenByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return getRce_examenByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen loadRce_examenByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Rce_examen) session.load(orm.Rce_examen.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen getRce_examenByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Rce_examen) session.get(orm.Rce_examen.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen loadRce_examenByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Rce_examen) session.load(orm.Rce_examen.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen getRce_examenByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Rce_examen) session.get(orm.Rce_examen.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRce_examen(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return queryRce_examen(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRce_examen(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return queryRce_examen(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen[] listRce_examenByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return listRce_examenByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen[] listRce_examenByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return listRce_examenByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRce_examen(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Rce_examen as Rce_examen");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRce_examen(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Rce_examen as Rce_examen");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Rce_examen", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen[] listRce_examenByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryRce_examen(session, condition, orderBy);
			return (Rce_examen[]) list.toArray(new Rce_examen[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen[] listRce_examenByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryRce_examen(session, condition, orderBy, lockMode);
			return (Rce_examen[]) list.toArray(new Rce_examen[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen loadRce_examenByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return loadRce_examenByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen loadRce_examenByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return loadRce_examenByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen loadRce_examenByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Rce_examen[] rce_examens = listRce_examenByQuery(session, condition, orderBy);
		if (rce_examens != null && rce_examens.length > 0)
			return rce_examens[0];
		else
			return null;
	}
	
	public static Rce_examen loadRce_examenByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Rce_examen[] rce_examens = listRce_examenByQuery(session, condition, orderBy, lockMode);
		if (rce_examens != null && rce_examens.length > 0)
			return rce_examens[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateRce_examenByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return iterateRce_examenByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateRce_examenByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return iterateRce_examenByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateRce_examenByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Rce_examen as Rce_examen");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateRce_examenByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Rce_examen as Rce_examen");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Rce_examen", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen createRce_examen() {
		return new orm.Rce_examen();
	}
	
	public static boolean save(orm.Rce_examen rce_examen) throws PersistentException {
		try {
			orm.SintítuloPersistentManager.instance().saveObject(rce_examen);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.Rce_examen rce_examen) throws PersistentException {
		try {
			orm.SintítuloPersistentManager.instance().deleteObject(rce_examen);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Rce_examen rce_examen)throws PersistentException {
		try {
			if (rce_examen.getProcedencia() != null) {
				rce_examen.getProcedencia().rce_examen.remove(rce_examen);
			}
			
			if (rce_examen.getReserva() != null) {
				rce_examen.getReserva().setRce_examen(null);
			}
			
			if (rce_examen.getPersona() != null) {
				rce_examen.getPersona().rce_examen.remove(rce_examen);
			}
			
			orm.Pago[] lPagos = rce_examen.pago.toArray();
			for(int i = 0; i < lPagos.length; i++) {
				lPagos[i].setRce_examen(null);
			}
			orm.Rce_examen_examen[] lRce_examen_examens = rce_examen.rce_examen_examen.toArray();
			for(int i = 0; i < lRce_examen_examens.length; i++) {
				lRce_examen_examens[i].setRce_examen(null);
			}
			orm.Bitacora[] lBitacoras = rce_examen.bitacora.toArray();
			for(int i = 0; i < lBitacoras.length; i++) {
				lBitacoras[i].setRce_examen(null);
			}
			return delete(rce_examen);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Rce_examen rce_examen, org.orm.PersistentSession session)throws PersistentException {
		try {
			if (rce_examen.getProcedencia() != null) {
				rce_examen.getProcedencia().rce_examen.remove(rce_examen);
			}
			
			if (rce_examen.getReserva() != null) {
				rce_examen.getReserva().setRce_examen(null);
			}
			
			if (rce_examen.getPersona() != null) {
				rce_examen.getPersona().rce_examen.remove(rce_examen);
			}
			
			orm.Pago[] lPagos = rce_examen.pago.toArray();
			for(int i = 0; i < lPagos.length; i++) {
				lPagos[i].setRce_examen(null);
			}
			orm.Rce_examen_examen[] lRce_examen_examens = rce_examen.rce_examen_examen.toArray();
			for(int i = 0; i < lRce_examen_examens.length; i++) {
				lRce_examen_examens[i].setRce_examen(null);
			}
			orm.Bitacora[] lBitacoras = rce_examen.bitacora.toArray();
			for(int i = 0; i < lBitacoras.length; i++) {
				lBitacoras[i].setRce_examen(null);
			}
			try {
				session.delete(rce_examen);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.Rce_examen rce_examen) throws PersistentException {
		try {
			orm.SintítuloPersistentManager.instance().getSession().refresh(rce_examen);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.Rce_examen rce_examen) throws PersistentException {
		try {
			orm.SintítuloPersistentManager.instance().getSession().evict(rce_examen);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
