/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Hora_medica {
	public Hora_medica() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_HORA_MEDICA_MEDICO) {
			this.medico = (orm.Medico) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_HORA_MEDICA_BOX) {
			this.box = (orm.Box) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_HORA_MEDICA_RESERVA) {
			this.reserva = (orm.Reserva) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private orm.Box box;
	
	private orm.Medico medico;
	
	private java.sql.Timestamp hora_inicio;
	
	private int tipo_hora;
	
	private Integer tiempo_periodo = new Integer(15);
	
	private orm.Reserva reserva;
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setHora_inicio(java.sql.Timestamp value) {
		this.hora_inicio = value;
	}
	
	public java.sql.Timestamp getHora_inicio() {
		return hora_inicio;
	}
	
	public void setTipo_hora(int value) {
		this.tipo_hora = value;
	}
	
	public int getTipo_hora() {
		return tipo_hora;
	}
	
	public void setTiempo_periodo(int value) {
		setTiempo_periodo(new Integer(value));
	}
	
	public void setTiempo_periodo(Integer value) {
		this.tiempo_periodo = value;
	}
	
	public Integer getTiempo_periodo() {
		return tiempo_periodo;
	}
	
	public void setMedico(orm.Medico value) {
		if (medico != null) {
			medico.hora_medica.remove(this);
		}
		if (value != null) {
			value.hora_medica.add(this);
		}
	}
	
	public orm.Medico getMedico() {
		return medico;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Medico(orm.Medico value) {
		this.medico = value;
	}
	
	private orm.Medico getORM_Medico() {
		return medico;
	}
	
	public void setBox(orm.Box value) {
		if (box != null) {
			box.hora_medica.remove(this);
		}
		if (value != null) {
			value.hora_medica.add(this);
		}
	}
	
	public orm.Box getBox() {
		return box;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Box(orm.Box value) {
		this.box = value;
	}
	
	private orm.Box getORM_Box() {
		return box;
	}
	
	public void setReserva(orm.Reserva value) {
		if (this.reserva != value) {
			orm.Reserva lreserva = this.reserva;
			this.reserva = value;
			if (value != null) {
				reserva.setHora_medica(this);
			}
			if (lreserva != null && lreserva.getHora_medica() == this) {
				lreserva.setHora_medica(null);
			}
		}
	}
	
	public orm.Reserva getReserva() {
		return reserva;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
