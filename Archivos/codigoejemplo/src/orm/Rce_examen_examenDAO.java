/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class Rce_examen_examenDAO {
	public static Rce_examen_examen loadRce_examen_examenByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return loadRce_examen_examenByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen_examen getRce_examen_examenByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return getRce_examen_examenByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen_examen loadRce_examen_examenByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return loadRce_examen_examenByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen_examen getRce_examen_examenByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return getRce_examen_examenByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen_examen loadRce_examen_examenByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Rce_examen_examen) session.load(orm.Rce_examen_examen.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen_examen getRce_examen_examenByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Rce_examen_examen) session.get(orm.Rce_examen_examen.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen_examen loadRce_examen_examenByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Rce_examen_examen) session.load(orm.Rce_examen_examen.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen_examen getRce_examen_examenByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Rce_examen_examen) session.get(orm.Rce_examen_examen.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRce_examen_examen(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return queryRce_examen_examen(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRce_examen_examen(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return queryRce_examen_examen(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen_examen[] listRce_examen_examenByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return listRce_examen_examenByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen_examen[] listRce_examen_examenByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return listRce_examen_examenByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRce_examen_examen(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Rce_examen_examen as Rce_examen_examen");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryRce_examen_examen(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Rce_examen_examen as Rce_examen_examen");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Rce_examen_examen", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen_examen[] listRce_examen_examenByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryRce_examen_examen(session, condition, orderBy);
			return (Rce_examen_examen[]) list.toArray(new Rce_examen_examen[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen_examen[] listRce_examen_examenByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryRce_examen_examen(session, condition, orderBy, lockMode);
			return (Rce_examen_examen[]) list.toArray(new Rce_examen_examen[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen_examen loadRce_examen_examenByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return loadRce_examen_examenByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen_examen loadRce_examen_examenByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return loadRce_examen_examenByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen_examen loadRce_examen_examenByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Rce_examen_examen[] rce_examen_examens = listRce_examen_examenByQuery(session, condition, orderBy);
		if (rce_examen_examens != null && rce_examen_examens.length > 0)
			return rce_examen_examens[0];
		else
			return null;
	}
	
	public static Rce_examen_examen loadRce_examen_examenByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Rce_examen_examen[] rce_examen_examens = listRce_examen_examenByQuery(session, condition, orderBy, lockMode);
		if (rce_examen_examens != null && rce_examen_examens.length > 0)
			return rce_examen_examens[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateRce_examen_examenByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return iterateRce_examen_examenByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateRce_examen_examenByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return iterateRce_examen_examenByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateRce_examen_examenByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Rce_examen_examen as Rce_examen_examen");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateRce_examen_examenByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Rce_examen_examen as Rce_examen_examen");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Rce_examen_examen", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Rce_examen_examen createRce_examen_examen() {
		return new orm.Rce_examen_examen();
	}
	
	public static boolean save(orm.Rce_examen_examen rce_examen_examen) throws PersistentException {
		try {
			orm.SintítuloPersistentManager.instance().saveObject(rce_examen_examen);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.Rce_examen_examen rce_examen_examen) throws PersistentException {
		try {
			orm.SintítuloPersistentManager.instance().deleteObject(rce_examen_examen);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Rce_examen_examen rce_examen_examen)throws PersistentException {
		try {
			if (rce_examen_examen.getRce_examen() != null) {
				rce_examen_examen.getRce_examen().rce_examen_examen.remove(rce_examen_examen);
			}
			
			if (rce_examen_examen.getExamen() != null) {
				rce_examen_examen.getExamen().rce_examen_examen.remove(rce_examen_examen);
			}
			
			return delete(rce_examen_examen);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Rce_examen_examen rce_examen_examen, org.orm.PersistentSession session)throws PersistentException {
		try {
			if (rce_examen_examen.getRce_examen() != null) {
				rce_examen_examen.getRce_examen().rce_examen_examen.remove(rce_examen_examen);
			}
			
			if (rce_examen_examen.getExamen() != null) {
				rce_examen_examen.getExamen().rce_examen_examen.remove(rce_examen_examen);
			}
			
			try {
				session.delete(rce_examen_examen);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.Rce_examen_examen rce_examen_examen) throws PersistentException {
		try {
			orm.SintítuloPersistentManager.instance().getSession().refresh(rce_examen_examen);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.Rce_examen_examen rce_examen_examen) throws PersistentException {
		try {
			orm.SintítuloPersistentManager.instance().getSession().evict(rce_examen_examen);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
