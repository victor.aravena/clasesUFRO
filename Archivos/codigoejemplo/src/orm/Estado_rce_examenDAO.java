/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class Estado_rce_examenDAO {
	public static Estado_rce_examen loadEstado_rce_examenByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return loadEstado_rce_examenByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Estado_rce_examen getEstado_rce_examenByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return getEstado_rce_examenByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Estado_rce_examen loadEstado_rce_examenByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return loadEstado_rce_examenByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Estado_rce_examen getEstado_rce_examenByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return getEstado_rce_examenByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Estado_rce_examen loadEstado_rce_examenByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Estado_rce_examen) session.load(orm.Estado_rce_examen.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Estado_rce_examen getEstado_rce_examenByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Estado_rce_examen) session.get(orm.Estado_rce_examen.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Estado_rce_examen loadEstado_rce_examenByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Estado_rce_examen) session.load(orm.Estado_rce_examen.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Estado_rce_examen getEstado_rce_examenByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Estado_rce_examen) session.get(orm.Estado_rce_examen.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryEstado_rce_examen(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return queryEstado_rce_examen(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryEstado_rce_examen(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return queryEstado_rce_examen(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Estado_rce_examen[] listEstado_rce_examenByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return listEstado_rce_examenByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Estado_rce_examen[] listEstado_rce_examenByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return listEstado_rce_examenByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryEstado_rce_examen(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Estado_rce_examen as Estado_rce_examen");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryEstado_rce_examen(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Estado_rce_examen as Estado_rce_examen");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Estado_rce_examen", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Estado_rce_examen[] listEstado_rce_examenByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryEstado_rce_examen(session, condition, orderBy);
			return (Estado_rce_examen[]) list.toArray(new Estado_rce_examen[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Estado_rce_examen[] listEstado_rce_examenByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryEstado_rce_examen(session, condition, orderBy, lockMode);
			return (Estado_rce_examen[]) list.toArray(new Estado_rce_examen[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Estado_rce_examen loadEstado_rce_examenByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return loadEstado_rce_examenByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Estado_rce_examen loadEstado_rce_examenByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return loadEstado_rce_examenByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Estado_rce_examen loadEstado_rce_examenByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Estado_rce_examen[] estado_rce_examens = listEstado_rce_examenByQuery(session, condition, orderBy);
		if (estado_rce_examens != null && estado_rce_examens.length > 0)
			return estado_rce_examens[0];
		else
			return null;
	}
	
	public static Estado_rce_examen loadEstado_rce_examenByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Estado_rce_examen[] estado_rce_examens = listEstado_rce_examenByQuery(session, condition, orderBy, lockMode);
		if (estado_rce_examens != null && estado_rce_examens.length > 0)
			return estado_rce_examens[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateEstado_rce_examenByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return iterateEstado_rce_examenByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateEstado_rce_examenByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return iterateEstado_rce_examenByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateEstado_rce_examenByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Estado_rce_examen as Estado_rce_examen");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateEstado_rce_examenByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Estado_rce_examen as Estado_rce_examen");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Estado_rce_examen", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Estado_rce_examen createEstado_rce_examen() {
		return new orm.Estado_rce_examen();
	}
	
	public static boolean save(orm.Estado_rce_examen estado_rce_examen) throws PersistentException {
		try {
			orm.SintítuloPersistentManager.instance().saveObject(estado_rce_examen);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.Estado_rce_examen estado_rce_examen) throws PersistentException {
		try {
			orm.SintítuloPersistentManager.instance().deleteObject(estado_rce_examen);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Estado_rce_examen estado_rce_examen)throws PersistentException {
		try {
			orm.Bitacora[] lBitacoras = estado_rce_examen.bitacora.toArray();
			for(int i = 0; i < lBitacoras.length; i++) {
				lBitacoras[i].setEstado_rce_examen(null);
			}
			return delete(estado_rce_examen);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Estado_rce_examen estado_rce_examen, org.orm.PersistentSession session)throws PersistentException {
		try {
			orm.Bitacora[] lBitacoras = estado_rce_examen.bitacora.toArray();
			for(int i = 0; i < lBitacoras.length; i++) {
				lBitacoras[i].setEstado_rce_examen(null);
			}
			try {
				session.delete(estado_rce_examen);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.Estado_rce_examen estado_rce_examen) throws PersistentException {
		try {
			orm.SintítuloPersistentManager.instance().getSession().refresh(estado_rce_examen);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.Estado_rce_examen estado_rce_examen) throws PersistentException {
		try {
			orm.SintítuloPersistentManager.instance().getSession().evict(estado_rce_examen);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
