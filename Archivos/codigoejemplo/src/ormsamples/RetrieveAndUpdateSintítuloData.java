/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class RetrieveAndUpdateSintítuloData {
	public void retrieveAndUpdateTestData() throws PersistentException {
		PersistentTransaction t = orm.SintítuloPersistentManager.instance().getSession().beginTransaction();
		try {
			orm.Box lormBox = orm.BoxDAO.loadBoxByQuery(null, null);
			// Update the properties of the persistent object
			orm.BoxDAO.save(lormBox);
			orm.Paciente lormPaciente = orm.PacienteDAO.loadPacienteByQuery(null, null);
			// Update the properties of the persistent object
			orm.PacienteDAO.save(lormPaciente);
			orm.Persona lormPersona = orm.PersonaDAO.loadPersonaByQuery(null, null);
			// Update the properties of the persistent object
			orm.PersonaDAO.save(lormPersona);
			orm.Director lormDirector = orm.DirectorDAO.loadDirectorByQuery(null, null);
			// Update the properties of the persistent object
			orm.DirectorDAO.save(lormDirector);
			orm.Hora_medica lormHora_medica = orm.Hora_medicaDAO.loadHora_medicaByQuery(null, null);
			// Update the properties of the persistent object
			orm.Hora_medicaDAO.save(lormHora_medica);
			orm.Medico lormMedico = orm.MedicoDAO.loadMedicoByQuery(null, null);
			// Update the properties of the persistent object
			orm.MedicoDAO.save(lormMedico);
			orm.Reserva lormReserva = orm.ReservaDAO.loadReservaByQuery(null, null);
			// Update the properties of the persistent object
			orm.ReservaDAO.save(lormReserva);
			orm.Especialidad lormEspecialidad = orm.EspecialidadDAO.loadEspecialidadByQuery(null, null);
			// Update the properties of the persistent object
			orm.EspecialidadDAO.save(lormEspecialidad);
			orm.Especialidad_medico lormEspecialidad_medico = orm.Especialidad_medicoDAO.loadEspecialidad_medicoByQuery(null, null);
			// Update the properties of the persistent object
			orm.Especialidad_medicoDAO.save(lormEspecialidad_medico);
			orm.Examen lormExamen = orm.ExamenDAO.loadExamenByQuery(null, null);
			// Update the properties of the persistent object
			orm.ExamenDAO.save(lormExamen);
			orm.Procedencia lormProcedencia = orm.ProcedenciaDAO.loadProcedenciaByQuery(null, null);
			// Update the properties of the persistent object
			orm.ProcedenciaDAO.save(lormProcedencia);
			orm.Pago lormPago = orm.PagoDAO.loadPagoByQuery(null, null);
			// Update the properties of the persistent object
			orm.PagoDAO.save(lormPago);
			orm.Formapago lormFormapago = orm.FormapagoDAO.loadFormapagoByQuery(null, null);
			// Update the properties of the persistent object
			orm.FormapagoDAO.save(lormFormapago);
			orm.Rce_examen lormRce_examen = orm.Rce_examenDAO.loadRce_examenByQuery(null, null);
			// Update the properties of the persistent object
			orm.Rce_examenDAO.save(lormRce_examen);
			orm.Rce_examen_examen lormRce_examen_examen = orm.Rce_examen_examenDAO.loadRce_examen_examenByQuery(null, null);
			// Update the properties of the persistent object
			orm.Rce_examen_examenDAO.save(lormRce_examen_examen);
			orm.Bitacora lormBitacora = orm.BitacoraDAO.loadBitacoraByQuery(null, null);
			// Update the properties of the persistent object
			orm.BitacoraDAO.save(lormBitacora);
			orm.Estado_rce_examen lormEstado_rce_examen = orm.Estado_rce_examenDAO.loadEstado_rce_examenByQuery(null, null);
			// Update the properties of the persistent object
			orm.Estado_rce_examenDAO.save(lormEstado_rce_examen);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			RetrieveAndUpdateSintítuloData retrieveAndUpdateSintítuloData = new RetrieveAndUpdateSintítuloData();
			try {
				retrieveAndUpdateSintítuloData.retrieveAndUpdateTestData();
			}
			finally {
				orm.SintítuloPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
