/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class DeleteSintítuloData {
	public void deleteTestData() throws PersistentException {
		PersistentTransaction t = orm.SintítuloPersistentManager.instance().getSession().beginTransaction();
		try {
			orm.Box lormBox = orm.BoxDAO.loadBoxByQuery(null, null);
			// Delete the persistent object
			orm.BoxDAO.delete(lormBox);
			orm.Paciente lormPaciente = orm.PacienteDAO.loadPacienteByQuery(null, null);
			// Delete the persistent object
			orm.PacienteDAO.delete(lormPaciente);
			orm.Persona lormPersona = orm.PersonaDAO.loadPersonaByQuery(null, null);
			// Delete the persistent object
			orm.PersonaDAO.delete(lormPersona);
			orm.Director lormDirector = orm.DirectorDAO.loadDirectorByQuery(null, null);
			// Delete the persistent object
			orm.DirectorDAO.delete(lormDirector);
			orm.Hora_medica lormHora_medica = orm.Hora_medicaDAO.loadHora_medicaByQuery(null, null);
			// Delete the persistent object
			orm.Hora_medicaDAO.delete(lormHora_medica);
			orm.Medico lormMedico = orm.MedicoDAO.loadMedicoByQuery(null, null);
			// Delete the persistent object
			orm.MedicoDAO.delete(lormMedico);
			orm.Reserva lormReserva = orm.ReservaDAO.loadReservaByQuery(null, null);
			// Delete the persistent object
			orm.ReservaDAO.delete(lormReserva);
			orm.Especialidad lormEspecialidad = orm.EspecialidadDAO.loadEspecialidadByQuery(null, null);
			// Delete the persistent object
			orm.EspecialidadDAO.delete(lormEspecialidad);
			orm.Especialidad_medico lormEspecialidad_medico = orm.Especialidad_medicoDAO.loadEspecialidad_medicoByQuery(null, null);
			// Delete the persistent object
			orm.Especialidad_medicoDAO.delete(lormEspecialidad_medico);
			orm.Examen lormExamen = orm.ExamenDAO.loadExamenByQuery(null, null);
			// Delete the persistent object
			orm.ExamenDAO.delete(lormExamen);
			orm.Procedencia lormProcedencia = orm.ProcedenciaDAO.loadProcedenciaByQuery(null, null);
			// Delete the persistent object
			orm.ProcedenciaDAO.delete(lormProcedencia);
			orm.Pago lormPago = orm.PagoDAO.loadPagoByQuery(null, null);
			// Delete the persistent object
			orm.PagoDAO.delete(lormPago);
			orm.Formapago lormFormapago = orm.FormapagoDAO.loadFormapagoByQuery(null, null);
			// Delete the persistent object
			orm.FormapagoDAO.delete(lormFormapago);
			orm.Rce_examen lormRce_examen = orm.Rce_examenDAO.loadRce_examenByQuery(null, null);
			// Delete the persistent object
			orm.Rce_examenDAO.delete(lormRce_examen);
			orm.Rce_examen_examen lormRce_examen_examen = orm.Rce_examen_examenDAO.loadRce_examen_examenByQuery(null, null);
			// Delete the persistent object
			orm.Rce_examen_examenDAO.delete(lormRce_examen_examen);
			orm.Bitacora lormBitacora = orm.BitacoraDAO.loadBitacoraByQuery(null, null);
			// Delete the persistent object
			orm.BitacoraDAO.delete(lormBitacora);
			orm.Estado_rce_examen lormEstado_rce_examen = orm.Estado_rce_examenDAO.loadEstado_rce_examenByQuery(null, null);
			// Delete the persistent object
			orm.Estado_rce_examenDAO.delete(lormEstado_rce_examen);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			DeleteSintítuloData deleteSintítuloData = new DeleteSintítuloData();
			try {
				deleteSintítuloData.deleteTestData();
			}
			finally {
				orm.SintítuloPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
