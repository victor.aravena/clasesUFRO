/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class ListSintítuloData {
	private static final int ROW_COUNT = 100;
	
	public void listTestData() throws PersistentException {
		System.out.println("Listing Box...");
		orm.Box[] ormBoxs = orm.BoxDAO.listBoxByQuery(null, null);
		int length = Math.min(ormBoxs.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormBoxs[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Paciente...");
		orm.Paciente[] ormPacientes = orm.PacienteDAO.listPacienteByQuery(null, null);
		length = Math.min(ormPacientes.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormPacientes[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Persona...");
		orm.Persona[] ormPersonas = orm.PersonaDAO.listPersonaByQuery(null, null);
		length = Math.min(ormPersonas.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormPersonas[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Director...");
		orm.Director[] ormDirectors = orm.DirectorDAO.listDirectorByQuery(null, null);
		length = Math.min(ormDirectors.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormDirectors[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Hora_medica...");
		orm.Hora_medica[] ormHora_medicas = orm.Hora_medicaDAO.listHora_medicaByQuery(null, null);
		length = Math.min(ormHora_medicas.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormHora_medicas[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Medico...");
		orm.Medico[] ormMedicos = orm.MedicoDAO.listMedicoByQuery(null, null);
		length = Math.min(ormMedicos.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormMedicos[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Reserva...");
		orm.Reserva[] ormReservas = orm.ReservaDAO.listReservaByQuery(null, null);
		length = Math.min(ormReservas.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormReservas[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Especialidad...");
		orm.Especialidad[] ormEspecialidads = orm.EspecialidadDAO.listEspecialidadByQuery(null, null);
		length = Math.min(ormEspecialidads.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormEspecialidads[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Especialidad_medico...");
		orm.Especialidad_medico[] ormEspecialidad_medicos = orm.Especialidad_medicoDAO.listEspecialidad_medicoByQuery(null, null);
		length = Math.min(ormEspecialidad_medicos.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormEspecialidad_medicos[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Examen...");
		orm.Examen[] ormExamens = orm.ExamenDAO.listExamenByQuery(null, null);
		length = Math.min(ormExamens.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormExamens[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Procedencia...");
		orm.Procedencia[] ormProcedencias = orm.ProcedenciaDAO.listProcedenciaByQuery(null, null);
		length = Math.min(ormProcedencias.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormProcedencias[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Pago...");
		orm.Pago[] ormPagos = orm.PagoDAO.listPagoByQuery(null, null);
		length = Math.min(ormPagos.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormPagos[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Formapago...");
		orm.Formapago[] ormFormapagos = orm.FormapagoDAO.listFormapagoByQuery(null, null);
		length = Math.min(ormFormapagos.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormFormapagos[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Rce_examen...");
		orm.Rce_examen[] ormRce_examens = orm.Rce_examenDAO.listRce_examenByQuery(null, null);
		length = Math.min(ormRce_examens.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormRce_examens[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Rce_examen_examen...");
		orm.Rce_examen_examen[] ormRce_examen_examens = orm.Rce_examen_examenDAO.listRce_examen_examenByQuery(null, null);
		length = Math.min(ormRce_examen_examens.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormRce_examen_examens[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Bitacora...");
		orm.Bitacora[] ormBitacoras = orm.BitacoraDAO.listBitacoraByQuery(null, null);
		length = Math.min(ormBitacoras.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormBitacoras[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Estado_rce_examen...");
		orm.Estado_rce_examen[] ormEstado_rce_examens = orm.Estado_rce_examenDAO.listEstado_rce_examenByQuery(null, null);
		length = Math.min(ormEstado_rce_examens.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormEstado_rce_examens[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
	}
	
	public static void main(String[] args) {
		try {
			ListSintítuloData listSintítuloData = new ListSintítuloData();
			try {
				listSintítuloData.listTestData();
			}
			finally {
				orm.SintítuloPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
