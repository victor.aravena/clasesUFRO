/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package business;

import ormsamples.*;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.orm.*;
import vo.PacienteJson;
public class Paciente {
	private static final int ROW_COUNT = 100;
	/**
         * Busca objeto de paciente y entrega un json o xml como salida
         * @param formato Indica si el resultado se entrega en XMl o JSON
         * @return 
         */
	static public String listTestData(String formato) {
		
		String query = null;//"persona.apellido_paterno = 'Mariqueo' ";
		Collection<PacienteJson> pjson = 
                        new ArrayList<PacienteJson> ();
                
                orm.Paciente[] ormPacientes;
            try {
                ormPacientes = orm.PacienteDAO.
                        listPacienteByQuery(query, null);
            
		int length = Math.min(ormPacientes.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			pjson.add(PacienteJson.
                                createPaciente(
                                    ormPacientes[i].getId(),
                                    ormPacientes[i].getPersona().getNombres(), 
                                    ormPacientes[i].getPersona().getEmail(), 
                                    ormPacientes[i].getDescripcion()
                                )
                        );
                        
		}
		} catch (PersistentException ex) {
                Logger.getLogger(Paciente.class.getName()).log(Level.SEVERE, null, ex);
            }
                if("XML".equals(formato)){
                    XStream xstream = new XStream();
                    xstream.alias("paciente", PacienteJson.class);
                    String xml = xstream.toXML(pjson);
                     return xml;
                }
                else{
                    if ("JSON".equals(formato)) {
                        String json = new Gson().toJson(pjson);
                        return json;
                    } else {
                        return "";
                    }
                
                }
                
              
               
	}
	
	
}
