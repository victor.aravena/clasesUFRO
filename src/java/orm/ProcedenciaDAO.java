/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class ProcedenciaDAO {
	public static Procedencia loadProcedenciaByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return loadProcedenciaByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Procedencia getProcedenciaByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return getProcedenciaByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Procedencia loadProcedenciaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return loadProcedenciaByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Procedencia getProcedenciaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return getProcedenciaByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Procedencia loadProcedenciaByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Procedencia) session.load(orm.Procedencia.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Procedencia getProcedenciaByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (Procedencia) session.get(orm.Procedencia.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Procedencia loadProcedenciaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Procedencia) session.load(orm.Procedencia.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Procedencia getProcedenciaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Procedencia) session.get(orm.Procedencia.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryProcedencia(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return queryProcedencia(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryProcedencia(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return queryProcedencia(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Procedencia[] listProcedenciaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return listProcedenciaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Procedencia[] listProcedenciaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return listProcedenciaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryProcedencia(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Procedencia as Procedencia");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryProcedencia(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Procedencia as Procedencia");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Procedencia", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Procedencia[] listProcedenciaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryProcedencia(session, condition, orderBy);
			return (Procedencia[]) list.toArray(new Procedencia[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Procedencia[] listProcedenciaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryProcedencia(session, condition, orderBy, lockMode);
			return (Procedencia[]) list.toArray(new Procedencia[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Procedencia loadProcedenciaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return loadProcedenciaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Procedencia loadProcedenciaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return loadProcedenciaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Procedencia loadProcedenciaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Procedencia[] procedencias = listProcedenciaByQuery(session, condition, orderBy);
		if (procedencias != null && procedencias.length > 0)
			return procedencias[0];
		else
			return null;
	}
	
	public static Procedencia loadProcedenciaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Procedencia[] procedencias = listProcedenciaByQuery(session, condition, orderBy, lockMode);
		if (procedencias != null && procedencias.length > 0)
			return procedencias[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateProcedenciaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return iterateProcedenciaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateProcedenciaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.SintítuloPersistentManager.instance().getSession();
			return iterateProcedenciaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateProcedenciaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Procedencia as Procedencia");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateProcedenciaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.Procedencia as Procedencia");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Procedencia", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Procedencia createProcedencia() {
		return new orm.Procedencia();
	}
	
	public static boolean save(orm.Procedencia procedencia) throws PersistentException {
		try {
			orm.SintítuloPersistentManager.instance().saveObject(procedencia);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.Procedencia procedencia) throws PersistentException {
		try {
			orm.SintítuloPersistentManager.instance().deleteObject(procedencia);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Procedencia procedencia)throws PersistentException {
		try {
			orm.Rce_examen[] lRce_examens = procedencia.rce_examen.toArray();
			for(int i = 0; i < lRce_examens.length; i++) {
				lRce_examens[i].setProcedencia(null);
			}
			return delete(procedencia);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.Procedencia procedencia, org.orm.PersistentSession session)throws PersistentException {
		try {
			orm.Rce_examen[] lRce_examens = procedencia.rce_examen.toArray();
			for(int i = 0; i < lRce_examens.length; i++) {
				lRce_examens[i].setProcedencia(null);
			}
			try {
				session.delete(procedencia);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.Procedencia procedencia) throws PersistentException {
		try {
			orm.SintítuloPersistentManager.instance().getSession().refresh(procedencia);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.Procedencia procedencia) throws PersistentException {
		try {
			orm.SintítuloPersistentManager.instance().getSession().evict(procedencia);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
}
