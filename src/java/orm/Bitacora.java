/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Bitacora {
	public Bitacora() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_BITACORA_ESTADO_RCE_EXAMEN) {
			this.estado_rce_examen = (orm.Estado_rce_examen) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_BITACORA_RCE_EXAMEN) {
			this.rce_examen = (orm.Rce_examen) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_BITACORA_PERSONA) {
			this.persona = (orm.Persona) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private orm.Estado_rce_examen estado_rce_examen;
	
	private orm.Rce_examen rce_examen;
	
	private orm.Persona persona;
	
	private String observacion;
	
	/**
	 * Clave Primaria
	 */
	private void setId(int value) {
		this.id = value;
	}
	
	/**
	 * Clave Primaria
	 */
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	/**
	 * Observacion
	 */
	public void setObservacion(String value) {
		this.observacion = value;
	}
	
	/**
	 * Observacion
	 */
	public String getObservacion() {
		return observacion;
	}
	
	public void setEstado_rce_examen(orm.Estado_rce_examen value) {
		if (estado_rce_examen != null) {
			estado_rce_examen.bitacora.remove(this);
		}
		if (value != null) {
			value.bitacora.add(this);
		}
	}
	
	public orm.Estado_rce_examen getEstado_rce_examen() {
		return estado_rce_examen;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Estado_rce_examen(orm.Estado_rce_examen value) {
		this.estado_rce_examen = value;
	}
	
	private orm.Estado_rce_examen getORM_Estado_rce_examen() {
		return estado_rce_examen;
	}
	
	public void setRce_examen(orm.Rce_examen value) {
		if (rce_examen != null) {
			rce_examen.bitacora.remove(this);
		}
		if (value != null) {
			value.bitacora.add(this);
		}
	}
	
	public orm.Rce_examen getRce_examen() {
		return rce_examen;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Rce_examen(orm.Rce_examen value) {
		this.rce_examen = value;
	}
	
	private orm.Rce_examen getORM_Rce_examen() {
		return rce_examen;
	}
	
	public void setPersona(orm.Persona value) {
		if (persona != null) {
			persona.bitacora.remove(this);
		}
		if (value != null) {
			value.bitacora.add(this);
		}
	}
	
	public orm.Persona getPersona() {
		return persona;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Persona(orm.Persona value) {
		this.persona = value;
	}
	
	private orm.Persona getORM_Persona() {
		return persona;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
