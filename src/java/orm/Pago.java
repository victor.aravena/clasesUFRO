/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Pago {
	public Pago() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_PAGO_FORMAPAGO) {
			this.formapago = (orm.Formapago) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_PAGO_RCE_EXAMEN) {
			this.rce_examen = (orm.Rce_examen) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private orm.Formapago formapago;
	
	private orm.Rce_examen rce_examen;
	
	private double monto;
	
	private Double comprobante;
	
	/**
	 * Clave primaria
	 */
	private void setId(int value) {
		this.id = value;
	}
	
	/**
	 * Clave primaria
	 */
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	/**
	 * Monto
	 */
	public void setMonto(double value) {
		this.monto = value;
	}
	
	/**
	 * Monto
	 */
	public double getMonto() {
		return monto;
	}
	
	/**
	 * Comprobante
	 */
	public void setComprobante(double value) {
		setComprobante(new Double(value));
	}
	
	/**
	 * Comprobante
	 */
	public void setComprobante(Double value) {
		this.comprobante = value;
	}
	
	/**
	 * Comprobante
	 */
	public Double getComprobante() {
		return comprobante;
	}
	
	public void setFormapago(orm.Formapago value) {
		if (formapago != null) {
			formapago.pago.remove(this);
		}
		if (value != null) {
			value.pago.add(this);
		}
	}
	
	public orm.Formapago getFormapago() {
		return formapago;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Formapago(orm.Formapago value) {
		this.formapago = value;
	}
	
	private orm.Formapago getORM_Formapago() {
		return formapago;
	}
	
	public void setRce_examen(orm.Rce_examen value) {
		if (rce_examen != null) {
			rce_examen.pago.remove(this);
		}
		if (value != null) {
			value.pago.add(this);
		}
	}
	
	public orm.Rce_examen getRce_examen() {
		return rce_examen;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Rce_examen(orm.Rce_examen value) {
		this.rce_examen = value;
	}
	
	private orm.Rce_examen getORM_Rce_examen() {
		return rce_examen;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
