/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Procedencia {
	public Procedencia() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_PROCEDENCIA_RCE_EXAMEN) {
			return ORM_rce_examen;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private int id;
	
	private String descripcion;
	
	private java.util.Set ORM_rce_examen = new java.util.HashSet();
	
	/**
	 * Clave Primaria
	 */
	private void setId(int value) {
		this.id = value;
	}
	
	/**
	 * Clave Primaria
	 */
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	/**
	 * Descripcion
	 */
	public void setDescripcion(String value) {
		this.descripcion = value;
	}
	
	/**
	 * Descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	
	private void setORM_Rce_examen(java.util.Set value) {
		this.ORM_rce_examen = value;
	}
	
	private java.util.Set getORM_Rce_examen() {
		return ORM_rce_examen;
	}
	
	public final orm.Rce_examenSetCollection rce_examen = new orm.Rce_examenSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_PROCEDENCIA_RCE_EXAMEN, orm.ORMConstants.KEY_RCE_EXAMEN_PROCEDENCIA, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
