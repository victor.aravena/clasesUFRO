/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Formapago {
	public Formapago() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_FORMAPAGO_PAGO) {
			return ORM_pago;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private int id;
	
	private String descripcion;
	
	private java.util.Set ORM_pago = new java.util.HashSet();
	
	/**
	 * Clave primaria
	 */
	private void setId(int value) {
		this.id = value;
	}
	
	/**
	 * Clave primaria
	 */
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	/**
	 * Descripcion
	 */
	public void setDescripcion(String value) {
		this.descripcion = value;
	}
	
	/**
	 * Descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	
	private void setORM_Pago(java.util.Set value) {
		this.ORM_pago = value;
	}
	
	private java.util.Set getORM_Pago() {
		return ORM_pago;
	}
	
	public final orm.PagoSetCollection pago = new orm.PagoSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_FORMAPAGO_PAGO, orm.ORMConstants.KEY_PAGO_FORMAPAGO, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
