/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vo;

/**
 * AUTOR VICTOR EJEMPLO PROGRAMACION AVANZADA
 * @author dci
 */
public class PacienteJson {
    
    int id;
    String nombre;
    String mail;
    String detalle;
    /**
     * Metodo constructor privado
     * @param id
     * @param nombre
     * @param mail
     * @param detalle 
     */
    private PacienteJson(
    int id, String nombre,
            String mail,
            String detalle           
    ){
        this.id = id;
        this.nombre = nombre;
        this.mail = mail;
        this.detalle=detalle;
    }
    /**
     * Metodo para crear instancia
     * @param id
     * @param nombre
     * @param mail
     * @param detalle
     * @return 
     */
    static public PacienteJson createPaciente(
            int id, 
            String nombre,
            String mail,
            String detalle ){
     return new PacienteJson(id,nombre,mail,detalle);
    }
}
