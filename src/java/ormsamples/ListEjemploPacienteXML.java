/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.util.ArrayList;
import java.util.Collection;
import org.orm.*;
import vo.PacienteJson;
public class ListEjemploPacienteXML {
	private static final int ROW_COUNT = 100;
	
	public void listTestData() throws PersistentException {
		
		System.out.println("Listing Paciente...");
                String query = null;//"persona.apellido_paterno = 'Mariqueo' ";
		Collection<PacienteJson> pjson = 
                        new ArrayList<PacienteJson> ();
                
                orm.Paciente[] ormPacientes = orm.PacienteDAO.
                        listPacienteByQuery(query, null);
		int length = Math.min(ormPacientes.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormPacientes[i]);
                        System.out.println(ormPacientes[i].
                                getPersona());
                        System.out.println(ormPacientes[i].
                                getPersona().getNombres());
                        
                        pjson.add(PacienteJson.
                                createPaciente(
                                    ormPacientes[i].getId(),
                                    ormPacientes[i].getPersona().getNombres(), 
                                    ormPacientes[i].getPersona().getEmail(), 
                                    ormPacientes[i].getDescripcion()
                                )
                        );
                        
		}
		System.out.println(length + " record(s) retrieved.");
                
                XStream xstream = new XStream();
                xstream.alias("paciente", PacienteJson.class);
                String xml = xstream.toXML(pjson);
                System.out.println("XML=>"+xml);
                
                
              //  xml = xstream.toXML(ormPacientes);
               // System.out.println("XML ORM =>"+xml);
                //String json = new Gson().toJson(pjson);
		//System.out.println("JSON=>"+json);
	}
	
	public static void main(String[] args) {
		try {
			ListEjemploPacienteXML listSintítuloData = new ListEjemploPacienteXML();
			try {
				listSintítuloData.listTestData();
			}
			finally {
				orm.SintítuloPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
