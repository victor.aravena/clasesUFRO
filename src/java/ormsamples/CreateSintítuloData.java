/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class CreateSintítuloData {
	public void createTestData() throws PersistentException {
		PersistentTransaction t = orm.SintítuloPersistentManager.instance().getSession().beginTransaction();
		try {
			/*orm.Box lormBox = orm.BoxDAO.createBox();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : hora_medica
			orm.BoxDAO.save(lormBox);
                    */
			
			orm.Persona lormPersona = orm.PersonaDAO.
                                createPersona();
                        lormPersona.setApellido_materno("Huilcapan");
                        lormPersona.setApellido_paterno("Mariqueo");
                        lormPersona.setDirector(null);
                        lormPersona.setEmail("f.mariqueo01@ufromail.cl");
                        lormPersona.setNombres("Fabian Alejandro");
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : bitacora, rce_examen, reserva, director, paciente, medico
			orm.PersonaDAO.save(lormPersona);
                        
                        
                        orm.Paciente lormPaciente = orm.PacienteDAO.createPaciente();
                        lormPaciente.setPersona(lormPersona);
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : reserva, persona
			lormPaciente.setDescripcion("Soy Paciente 2");
                        orm.PacienteDAO.save(lormPaciente);
                        
			/*orm.Director lormDirector = orm.DirectorDAO.createDirector();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : persona
			orm.DirectorDAO.save(lormDirector);
			orm.Hora_medica lormHora_medica = orm.Hora_medicaDAO.createHora_medica();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : reserva, tipo_hora, medico, box
			orm.Hora_medicaDAO.save(lormHora_medica);
			orm.Medico lormMedico = orm.MedicoDAO.createMedico();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : especialidad_medico, hora_medica, persona
			orm.MedicoDAO.save(lormMedico);
			orm.Reserva lormReserva = orm.ReservaDAO.createReserva();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : rce_examen, hora_medica, persona_id_ingresa_reserva, paciente
			orm.ReservaDAO.save(lormReserva);
			orm.Especialidad lormEspecialidad = orm.EspecialidadDAO.createEspecialidad();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : especialidad_medico
			orm.EspecialidadDAO.save(lormEspecialidad);
			orm.Especialidad_medico lormEspecialidad_medico = orm.Especialidad_medicoDAO.createEspecialidad_medico();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : medico, especialidad
			orm.Especialidad_medicoDAO.save(lormEspecialidad_medico);
			orm.Examen lormExamen = orm.ExamenDAO.createExamen();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : rce_examen_examen
			orm.ExamenDAO.save(lormExamen);
			orm.Procedencia lormProcedencia = orm.ProcedenciaDAO.createProcedencia();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : rce_examen
			orm.ProcedenciaDAO.save(lormProcedencia);
			orm.Pago lormPago = orm.PagoDAO.createPago();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : monto, rce_examen, formapago
			orm.PagoDAO.save(lormPago);
			orm.Formapago lormFormapago = orm.FormapagoDAO.createFormapago();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : pago
			orm.FormapagoDAO.save(lormFormapago);
			orm.Rce_examen lormRce_examen = orm.Rce_examenDAO.createRce_examen();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : bitacora, rce_examen_examen, pago, persona, reserva, procedencia
			orm.Rce_examenDAO.save(lormRce_examen);
			orm.Rce_examen_examen lormRce_examen_examen = orm.Rce_examen_examenDAO.createRce_examen_examen();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : examen, rce_examen
			orm.Rce_examen_examenDAO.save(lormRce_examen_examen);
			orm.Bitacora lormBitacora = orm.BitacoraDAO.createBitacora();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : persona, rce_examen, estado_rce_examen
			orm.BitacoraDAO.save(lormBitacora);
			orm.Estado_rce_examen lormEstado_rce_examen = orm.Estado_rce_examenDAO.createEstado_rce_examen();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : bitacora
			orm.Estado_rce_examenDAO.save(lormEstado_rce_examen);
*/
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			CreateSintítuloData createSintítuloData = new CreateSintítuloData();
			try {
				createSintítuloData.createTestData();
			}
			finally {
				orm.SintítuloPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
